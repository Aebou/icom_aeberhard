<?php require_once('inc/config.php'); ?>

    <html>

   <!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="sheet" href="assets/css/normalize.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <link rel="stylesheet" href="assets/css/style.css">

    </head>

   <body>
    <div class="container">
      <?php require_once('template/header.php'); ?>
      <main>
      <ul class="tasklist">
        <li class="tasklist-item-principal">
          <span class="tasklist-item-id">ID</span>
            <span class="tasklist-item-name">Name</span>
          <span class="tasklist-item-mois">01</span>
          <span class="tasklist-item-mois">02</span>
          <span class="tasklist-item-mois">03</span>
          <span class="tasklist-item-mois">04</span>
          <span class="tasklist-item-mois">05</span>
          <span class="tasklist-item-mois">06</span>
          <span class="tasklist-item-mois">07</span>
          <span class="tasklist-item-mois">08</span>
          <span class="tasklist-item-mois">09</span>
          <span class="tasklist-item-mois">10</span>
          <span class="tasklist-item-mois">11</span>
          <span class="tasklist-item-mois">12</span>
          <span class="tasklist-item-supprimer"></span>
        </li>
        <?php foreach ($data as $row) : ?>
          <?php
          // Les délimiteurs peuvent être des tirets, points ou slash
          $datestart = $row['start'];
          $dateend = $row['end'];
          list($years, $months, $days) = split('[/.-]', $datestart);
          list($yeare, $monthe, $daye) = split('[/.-]', $dateend);

          ?>
        <li class="tasklist-item">
          <span class="tasklist-item-id"><?php echo $row['id']?></span>
          <span class="tasklist-item-name"><?php echo $row['name']?></span>
          <span class="tasklist-item-mois"><?php if($months == "01" or $monthe=="01"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "02" or $monthe=="02"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "03" or $monthe=="03"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "04" or $monthe=="04"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "05" or $monthe=="05"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "06" or $monthe=="05"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "07" or $monthe=="07"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "08" or $monthe=="08"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "09" or $monthe=="09"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "10" or $monthe=="10"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "11" or $monthe=="11"){echo "-";}?></span>
          <span class="tasklist-item-mois"><?php if($months == "12" or $monthe=="12"){echo "-";}?></span>

          <span class="tasklist-item-bouton"><a class="poubelle" href="delete.php?task=<?php echo $row['id'];?>"><i class="fa fa-trash" aria-hidden="true"></i></a></span>
        </li>
        <?php endForeach;?>
      </ul>

         <a class="add" href="edit.php"><i class="fa fa-plus-circle fa-4x" aria-hidden="true"></i></a>





                </main>

                <?php require_once('template/footer.php'); ?>

              </div>




            </body>
            </html>
